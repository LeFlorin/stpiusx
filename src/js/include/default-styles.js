// ================================
// DEFAULT STYLE 
// ================================

import {
    $tableStyle
} from './config-vars';

DEFAULT_STYLES = {

  init () {
    if ($tableStyle.length) {
      this.tableStyle();
    }
  },

  tableStyle () {
    $tableStyle.parent('.fsElementContent').addClass('table-overflow');
  },

}; //end DEFAULT_STYLES

export default DEFAULT_STYLES;
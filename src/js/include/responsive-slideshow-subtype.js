// ================================================
// Element Slideshow Subtype Responsive Breakdown
// ================================================

import {
    $slideshowSubtypeHorizontal
} from './config-vars';

RESPONSIVE_SLIDESHOW_SUBTYPE = {

    init () {

        this.responsiveSlideshows();

    }, 

    responsiveSlideshows () {

        // add responsive breakpoints to default element horizonal slideshow subtype 

        var options,
        // update breakpoint variables here
            bpOneSlide = 600,
            bpTwoSlides = 800,
            bpThreeSlides = 1000,
            bpFourSlides = 1100
        ;

        $slideshowSubtypeHorizontal.each( function ( index, element ){

            var _$carousel = $( element ).find( '.fsElementSlideshow');
            var slidesToShow = _$carousel.data('slides-to-show');

            //determines slides to show & and slides to scroll
            var stsTwo = slidesToShow < 2 ? slidesToShow : 2;
            var stsThree =  slidesToShow < 3 ? slidesToShow : 3;
            var stsFour = slidesToShow < 4 ? slidesToShow : 4;
            var stsFive =  slidesToShow < 5 ? slidesToShow : 5;


            options = {
                'arrows': true,
                'mobileFirst': true,
                'slidesToShow': 1,
                'slidesToScroll': 1,
                'adaptiveHeight': true,
                'responsive': [
                    {
                        'breakpoint': bpOneSlide,
                        'settings': {
                            'slidesToShow': stsTwo,
                            'slidesToScroll': stsTwo
                        }
                    },
                    {
                        'breakpoint': bpTwoSlides,
                        'settings': {
                            'slidesToShow': stsThree,
                            'slidesToScroll': stsThree
                        }
                    },
                    {
                        'breakpoint': bpThreeSlides,
                        'settings': {
                            'slidesToShow': stsFour,
                            'slidesToScroll': stsFour
                        }
                    },
                    {
                        'breakpoint': bpFourSlides,
                        'settings': {
                            'slidesToShow': stsFive,
                            'slidesToScroll': stsFive
                        }
                    },
                ]
            };


            _$carousel.slick('setOption', options, true);

        });
    }

}; //end RESPONSIVE_SLIDESHOW_SUBTYPE

export default RESPONSIVE_SLIDESHOW_SUBTYPE;
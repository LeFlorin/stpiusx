export	const $body = $('body'),
				$header = $('.fsHeader'),
				$section = $('section'),
				isHome = $('.home').length,
				notDraftMode = !$('.fsDraftMode').length, // if (isHome && notDraftMode)...
				notComposeMode = !$('.fsComposeMode').length,
				$window = $(window),

				// Constant Custom Classes
				// ================================,
				$navMain = $('.nav-main'),
				$navMain_level1 = $('#fsHeader').find('.nav-main .fsNavLevel1'),
				$navSub = $('.nav-sub'),
				sectionTitle = $navMain_level1.find('> li[class*="fsNavCurrentPage"] > a').text(),
				$navSub_title = $navSub.find('> header > .fsElementTitle'),
				$navMainMobile = $('.nav-main-mobile'),
				mobileMenuToggle = '.mobile-toggle',
				$mobileParentNavs = $navMainMobile.find('.fsNavParentPage'),

				// Element Specific Variables
				// ================================,
				$calendarGrid = $('.fsCalendar.fsGrid'),

				// Horizontal Slideshow 
				// ================================,
				$slideshowSubtypeHorizontal = $('.fsSlideshow.fsSlideshowHorizontal'),

				// Specific Custom Classes
				// ================================,
				$siteSearch = $('.site-search'),
				$searchToggle = $('.search-toggle'),

				// Styles Manager Variables
				// ================================
				$tableStyle = $('table.fs_style_XX, table.fsElementTable')		    	

				// Enhancements
				// ================================
				// $heroImageElement = $('.hero-image'),

	;
// ===================================
// FONT LOADER CODE
// uncomment out section below for use
// ===================================

// require('../internal/font-loader');
// // configure fonts to load from vendors
// // check src/js/internal/font-loader.js for reference
// WebFontConfig = {
//  typekit: {
//    id: 'ycj7wse'
//  }
// };


// ===================================
// IMPORT INCLUDE/ METHODS
// ===================================

import {
    isHome,
    notDraftMode,
    notComposeMode,
    $calendarGrid,
    $slideshowSubtypeHorizontal
} from './config-vars';

import RESPONSIVE_CALENDAR_GRID from '../internal/responsive-calendar-grid';
import RESPONSIVE_SLIDESHOW_SUBTYPE from './responsive-slideshow-subtype';
import ACCESSIBILITY from './accessibility';
import NAVIGATION from './navigation';
import DEFAULT_STYLES from './default-styles';
import ENHANCEMENTS from './enhancements';
import HOME from './home';

INITIATE_ALL = {
	init () {

		if (notDraftMode) {
			ACCESSIBILITY.init();
		}

		NAVIGATION.init();

        DEFAULT_STYLES.init();

        if (isHome) {
            HOME.init();
        }

        if (notComposeMode) {
        	ENHANCEMENTS.init();
        }

        if($calendarGrid.length) {
            RESPONSIVE_CALENDAR_GRID.init();
        }

        if($slideshowSubtypeHorizontal.length) {
            var checkSlider = setInterval(function() {
                var totalSlideshowLength = $slideshowSubtypeHorizontal.length;
                if( $('.fsSlideshowHorizontal .slick-initialized').length === totalSlideshowLength) {
                    clearInterval(checkSlider);
                    RESPONSIVE_SLIDESHOW_SUBTYPE.init();
                }
            },100);
        }
        
	}
}
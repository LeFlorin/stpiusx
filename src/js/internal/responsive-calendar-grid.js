// ===================================================
// Calendar Element GRid Subtype Responsive Breakdown
// ===================================================


RESPONSIVE_CALENDAR_GRID = {

    init () {

        this.responsiveCalendarGrid();

    }, 

    responsiveCalendarGrid () {
        var eventview,
        scrollUp,

        onClickGridEvent = function onClickGridEvent(event){
            var $this = $(event.target).closest('.fsCalendarDaybox'),
                offsetTop,
                date
            ;

            date = $this.clone();
            offsetTop = eventview.offset().top - 16;

            $('.fsGrid .fsCalendarEventGrid .fsCalendarDaybox, .fsGrid .fsCalendarWeekendDayBox>div').removeClass('selected');

            eventview.empty().append(date);

            $this.addClass('selected');

            $('html,body').animate({scrollTop: offsetTop},450);
        },

        onClickScrollUp = function onClickScrollUp(){
            var offsetTop = $('.fsGrid .fsCalendarMonthBrowser').offset().top - 100;
            $('html,body').animate({scrollTop: offsetTop},450);
        },

        onAJAXSuccess = function onAJAXSuccess(event, xhr, options, data){

            var isCalendar = $(data).hasClass('fsCalendar fsGrid')
            ;

            if(isCalendar){
                initCalendar();
            }
        },

        initCalendar = function initCalendar(){
            eventview = $('<div id="event-view" />').insertAfter('.fsGrid .fsCalendarEventGrid');
            scrollUp = $('<div class="scroll-up"><span>Back Up To Calendar</span></div>').insertAfter(eventview);

            scrollUp.on('click', onClickScrollUp);

            $('.fsGrid .fsCalendarDaybox').has('.fsCalendarInfo').addClass('has-info');
            //event-view
            $('.fsGrid .fsCalendarEventGrid').on('click','.fsCalendarDaybox:not(.fsCalendarWeekendDayBox),.fsCalendarWeekendDayBox>div ', onClickGridEvent);
        }

        $(document).ajaxSuccess(onAJAXSuccess);
        initCalendar();     
    }

}; //end RESPONSIVE_CALENDAR_GRID

export default RESPONSIVE_CALENDAR_GRID;
// =============================
// Children Focus Affects Parent
// =============================

// This function is to handle situations where content is hidden, like modals or slider content
// Used properly, this function will detect when focus'able children (anchors, inputs, etc) are tabbed onto (focused)
// You can run functions when this child is focused to affect the parent (or theoretically any element)
  // E.g. add a class to the parent to slide the content up, open modals, etc
  // Demo Page - http://coderepo.demo.finalsite.com/keyboard-inner-anchor-element

// There are 3 ways to call this depending on how you want to set this up
/*
  ======================
  $('a, input, .example-child').parentFocus('.parent-element-of-previous-selector', function () {
    // function to run on focus
  }, function() {
    // function to run on blur
  });
  ======================
  function focusFunc() {
    // function to run on focus
  }
  function blurFunc() {
    // function to run on blur
  }
  $('a, input, .example-child').parentFocus('.parent-element-of-previous-selector', focusFunc, blurFunc);
  ======================
  function focusFunc(ele) {
    // function to run on focus
  }
  function blurFunc(ele) {
    // function to run on blur
  }
  $('a, input, .example-child').parentFocus('.parent-element-of-previous-selector', function() {
    focusFunc($(this));
  },
  function() {
    blurFunc($(this));
  });
  ======================
*/


;(function($){

  "use strict";

  $.fn.parentFocus = function focusToggle(selector, focusFn, blurFn) {

    return this.each(function () {
      var $this = $(this);
      var $closest = $this.closest(selector);

      $this
        .focus(function() {
          $closest.each(focusFn)
        })
        .blur(function() {
          $closest.each(blurFn || focusFn)
        });
    });

  }

}(jQuery));
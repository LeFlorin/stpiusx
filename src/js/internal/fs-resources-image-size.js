// ====================================================
// Resources - Get Specific Image Size Function
// ====================================================

/* v1.0.0

  Usage: getImageSize(img,size)

  - img should be a Resources img element selected with jQuery (the function requires a jQuery object)
  - size can be the following:
    - "large" (default), returns the largest width available for the image
    - "small", returns the smallest width available for the image
    - a number (pixel width), will return the width that is the next widest than then number
      Example: 480 will return the image that is 512

  Import function within desired file for use: 

  import {
    getImageSize
  } from '../internal/fs-resources-image-size';

  Example of selecting an img and using getImageSize to define the src: 

  $(function(){
    var myImg = $('.menu img').eq(0);
    myImg.attr('src', getImageSize(myImg,480));
  });
  
*/

export function getImageSize(img,size) {

  if (img.length === 0) {return false;}

  var customSrc, 
      sizeData = JSON.parse(unescape(img[0].getAttribute('data-image-sizes')));
  
  if (typeof size === 'number') {
    for (i=0; i < sizeData.length; i++) {
      if (customSrc === undefined && sizeData[i].width >= size) {
        customSrc = sizeData[i].url;
      }
    }
  }

  if (customSrc) {
    return customSrc;
  } else if (size === 'small') {
    return sizeData[0].url;
  } else {
    return sizeData[ sizeData.length-1 ].url;
  }
  
};

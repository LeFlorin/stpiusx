// /*!
//  * global_vars is a file particular to your site
//  * it contains base functions that are likely but not always used
//  **/

// check for buildinfo and add classes to body tag
(function(){

    if ( window.buildinfo !== undefined ){
        var b = document.getElementsByTagName('body')[0]; 
        b.setAttribute( 'data-buildver', window.buildinfo.ver );
        b.setAttribute( 'data-sitetemplate', window.buildinfo.template );

        /*
             // how to style against certain versions
             body[data-buildver^="2.0"]{
             margin-top: 100px;
             }
         */
    }

})();

require('./vendor/modernizr.custom.80802');
require('./include/index');


INITIATE_ALL.init();


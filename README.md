Finalsite Webpack Build
========================

## Instructions for Initial Setup

**Make sure you have nvm (node version manager) installed**

> package manager MAC: https://github.com/creationix/nvm
> PC: https://github.com/coreybutler/nvm-windows

1. Install/Update Node.js - www.nodejs.org

> check node version ($ node -v) and make sure it is above v8.6.0 or is the most recent LTS (long term support) version of node.js

2. Make sure npm is up to date

> check npm version ($ npm -v) and make sure it is above v4.6.0 or the lastest permissable version (this is where a versioning manager is very helpful and should make using the 'sudo' part of your command unnecessary)

```
$ npm update -g npm
```

3. Install Webpack CLI
```
$ npm install -g webpack
```

4. Install Sass
```
$ gem install sass
```
<br /><br />

## Forking the repository

1. Go to fs-webpack-build in Bitbucket https://stash.fs4.us/projects/DPL/repos/fs-webpack-build/browse and select 'Fork'

2. Fork Settings
```
Project: Deployment - Clients

Name: sitename

* uncheck 'Enable Fork Syncing'
```

3. Checkout newly created reposity in source tree or clone to its own folder and begin
<br /><br />


## Onto your Project
1. In the terminal
```
$ cd path/to/project/folder
```
>branches (ex: clients/schoolname) are now deprecated and each school will live within its own repository moving forward
>Note: any naming convention in git should be all lowercase and use the unique sitename

2. Delete node_modules folder and install all of the npm package dependencies
```
$ npm install
```
>if you run into errors at this step, please make sure you completed
intial instructions. if it has been a while since you updated npm, go
ahead and do that now.

3. Set up your Site Config in package.json
```
  "config": {
    "name": "Your Name", //replace with your name
    "pm": "Project Managaer", //replace with PM
    "designer": "Site Designer", //replace with the designer
    "title": "School Name", //replace with client name
    "server": "cfXX.fs4.us", //replace with server name (ex: cf50.fs4.us)
    "sitename": "sitename", //replace with sitename (this is unique to every school)
    "serverpath": "", //use "demo" or "templates" for sites on cf50
    "themepath": "default_18", //replace with name of theme folder
    "template": "newclientcustom", // change this to the town name if it's a base theme site
    "siteurl": "http://sitename.finalsite.com", //replace with client url
    "startpage": "production", //page to load on open:site
    "district": false, //set to true if you have a district site
    "pcdrive": "G:" //windows users only - drive to map projec to
  },
```

4. If you have a district site, you will need to add your schools in district-vars.js before running any commands, please see https://stash.fs4.us/projects/DPL/repos/fs-webpack-build/browse/docs/district-documentation.md for further instruction

<br /><br />
## In your terminal

**IMPORTANT PC USERS: executing ‘npm run publish’, ‘npm run map’, or ‘name run map-remove’ in your terminal will delete whatever letter drive is specified in the config section of your package.json file. Please be aware of this and change the letter if you do not want to delete the mapped drive**

1. you must publish files to the live/production environment on your first command to create your files & and update your theme in composer
```
$ npm run publish
```

2. If you weren't initially connected to the server (mac) or your letter drive was in use (pc) , you will need to run the command again:
```
$ npm run publish
```
<br />

**For your very first command when starting a new project you must execute 'npm run publish', once that is completed you can exit out and start using the development environment.**

<br /><br />

## ENVIRONMENTS:

**The DEVELOPMENT environment:** ($ npm start)
Once you are initially setup in composer and have published your inital files, you have the option to work locally! this means no connecting to the server/VPN OR working on a live site to see real-time updates to your site. You should notice a faster compilation time & rendering as well. 

> any updates and changes made in this environment will only be visible to YOU. You must publish to the production environment in order for anyone to see your changes

> saved changes in sass will be injected into your browser on localhost:3000, saved changes to js files will force a refresh

**The PRODUCTION environment:** ($ npm run publish)
Once you are initially setup in composer, you will now be working on your live site. 'npm run publish' will compile all your files, upload them to the server, and then trigger browserSync to create a new "proxy" server based on your config.siteurl.startpage

> the production environment requires being connected to the VPN if you are remote

> saved changes in sass or js will force a refresh in localhost:5000 and this may take a bit longer to render because it's saving to the live server. 

<br />

Option to open admin/fs login page as well as site startpage:

```
$ npm start
```
     
Option to just open site startpage in browser:

```
$ npm run local
```

<br />
After you have run your initial setup and published your files to the server, it will be easy to jump back into a project by running 'npm start' or 'npm run local', **It will be imperative that you 'npm run publish' your updates in order for them to render on the live site**<br /><br />
'npm start' and 'npm run local' will compile all your files, and NOT upload them to the server so you will be working in a local environment<br /><br />


## General Notes:

- In your console/terminal, browserSync will spit out both a localhost proxy you can reach your site at, or an external.
internal (development environment - localhost:3000, production environment - localhost:5000) & external url (XXX.XX.X.XX:XXXX). The external can be used on other devices not connected to your computer (phones, other devices), while localhost will run in all local browsers.<br />

- Just visit localhost:3000 from your browser, and you will see your site. Use this as the FRONT END view while you style your site to see instant changes.<br />

- Visit the external URL from your phone (make sure you are on fsCorp if local) and see instant changes from that device as well. No browser extensions needed.<br />


// Required Components
// ==========================================================================
const ev = require('../export-vars.js');
const path = require('path');

const merge = require('webpack-merge');
const common = require('../webpack/webpack.common.js');
const hotMiddlewareScript = 'webpack-hot-middleware/client?reload=true';

const dv = require('./district-vars.js');


const webpackconfig = merge(common, {

  entry: dv.entryDEV,

  // devtool: 'inline-cheap-module-source-map',
  devtool: 'none',

  plugins: [

    new ev.wp.HotModuleReplacementPlugin()

  ],

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ev.ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', 
              options: {
                sourceMap: false
              }
            },
            { loader: 'postcss-loader', options: {sourceMap: false}},
            { loader: 'resolve-url-loader'},
            { loader: 'sass-loader',
              options: {
                data: '@import "include";',
                sourceMap: true,
                includePaths: [
                  path.resolve(__dirname, "../src/sass/"),
                  path.resolve(__dirname, "../src/district/sass")
                ]
              }
            }
          ]
        })
      }
    ],
  },

  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].js',
    publicPath: '/uploaded/themes/' + ev.pkg.config.themepath
  }

});

module.exports = webpackconfig;